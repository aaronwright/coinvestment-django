
from django.contrib import admin
from django.urls import path, include
from accounts.views import UserProfile, ProfileApiView

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('profile/', ProfileApiView.as_view()),
    path('api-auth/', include('rest_framework.urls')),
] 

