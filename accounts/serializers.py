from rest_framework import serializers 
from rest_framework.validators import UniqueValidator
from django.contrib.auth.models import User


from coins import models


class HelloSerializer(serializers.Serializer):
    """Serializes name field for testing APIView """

    name = serializers.CharField(max_length=10)



class ProfileSerializer(serializers.ModelSerializer):
    coin = serializers.SlugRelatedField(
    read_only=True,
    slug_field='name'
    )
    user = serializers.SlugRelatedField(
    read_only=True,
    slug_field='username'
    )
    
    class Meta:
        model = models.Purchase
        fields = ('coin', 'bought_at', 'amount', 'user')


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    username = serializers.CharField(
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    password = serializers.CharField(min_length=8, write_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'], validated_data['email'],
             validated_data['password'])
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
