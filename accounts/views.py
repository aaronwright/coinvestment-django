from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from coins.models import Purchase, Coin
from coins.forms import PurchaseForm, DeleteForm

from . import serializers
# DRF 
from rest_framework import status
from rest_framework.views import APIView 
from rest_framework.response import Response
from rest_framework import permissions


from accounts.permissions import IsOwner
# Create your views here.

@login_required
def UserProfile(request):

    purchases = Purchase.objects.filter(user=request.user).order_by('-id')


    if request.method == 'POST':
        # User is adding a new investment

        form = PurchaseForm(request.POST)

        if form.is_valid():
            coin = Coin.objects.get(id=request.POST['coin_id'])
            
            new_purchase = Purchase()
            new_purchase.coin = coin
            new_purchase.bought_at = form.cleaned_data['bought_at']
            new_purchase.amount = form.cleaned_data['amount']
            new_purchase.user = request.user
            new_purchase.save()
        return HttpResponseRedirect('/profile')


    form = PurchaseForm()
    delete_form = DeleteForm()


    total_portfolio_value = 0
    portfolio_difference = 0
    total_invested = 0

    coins_purchased = []

    for purchase in purchases:
        #Set difference for each purchase
        purchase.difference = (purchase.coin.current_price - purchase.bought_at) * purchase.amount
        purchased_at = (purchase.bought_at * purchase.amount)
        purchase.total_purchase_value = purchased_at + purchase.difference
        total_invested += purchased_at
        total_portfolio_value += (purchase.coin.current_price * purchase.amount)
        coins_purchased.append(purchase.coin)
        portfolio_difference += purchase.difference

    # Get unique coins
    coins_purchased = list(set(coins_purchased))

    aggregate_values = {}

    for x in coins_purchased:
        agg_val = 0
       
        for pur in Purchase.objects.filter(user=request.user, coin=x):
            agg_val += (pur.amount * pur.bought_at) + ((pur.coin.current_price - pur.bought_at) * pur.amount)

        aggregate_values[x.ticker] = agg_val

    # Get All Coins 

    coin_list = Coin.objects.all()

        
    
    return render(request, 'accounts/profile.html', {'purchases': purchases, 
                                                    'total_porfolio_value': total_portfolio_value,
                                                    'portfolio_difference': portfolio_difference,
                                                    'total_invested': total_invested,
                                                    'aggregate_values': aggregate_values,
                                                    'coin_list': coin_list,
                                                    'form': form,
                                                    'delete_form': delete_form})




@login_required
def DeletePurchase(request):
    if request.method == 'POST':
        form = DeleteForm(request.POST)

        if form.is_valid():
            coin_id = form.cleaned_data['id']
            purchase_to_delete = Purchase.objects.filter(id=coin_id,user=request.user)
            if purchase_to_delete:
                purchase_to_delete.delete()
                return HttpResponseRedirect('/profile')
            else:
                return HttpResponseRedirect('/')
                
        return HttpResponseRedirect('/')


    else:
        return HttpResponseRedirect('/asdfa')



# API - DRF 


class ProfileApiView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get(self, request, format=None):
        """ Returns a list of Purchases  """

        if request.user.is_authenticated:
            print(request.user)
            queryset = purchases = Purchase.objects.filter(user=request.user)
            serializer = serializers.ProfileSerializer(queryset, many=True)
            return Response({'message': 'Hello',
                        'purchases': serializer.data})
        else:
            return Response('Not logged in.', status=status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


