$(document).ready(function() {

$('.profile-delete').on('click', function(event) {
    event.preventDefault();
    var title = $(this).data("title");
    var coinId = $(this).data("coinid");
    var amount = $(this).data("amount");
    var boughtAt = $(this).data("price");    

    var idInput = $('#id_id');
    var deleteModalLabel = $("#deleteModalLabel");

    var deleteModalDetailElm = $("#deleteModalDetails");

    deleteModalLabelTxt = "Delete " + title + " Investment?";
    deleteModalTxtDetail = "Are you sure you want to stop tracking your investment of " + amount + " " + title + " purchased at $" + boughtAt + "?"; 

    deleteModalLabel.text(deleteModalLabelTxt);
    deleteModalDetailElm.text(deleteModalTxtDetail);

    idInput.attr('value', coinId);

});

$('.profile-add').on('click', function(event) {
    event.preventDefault();
    var title = $(this).data("title");
    var coinId = $(this).data("coinid");


    var idInput = $('#coin_id_hidden');
    var addModalLabel = $("#add_modal_title");

    var addHeaderTxt = 'Add a ' + title + ' Investment to Track.';

    idInput.attr('value', coinId);

    addModalLabel.text(addHeaderTxt);

    idInput.attr('value', coinId);

});



}); 