from django.contrib import admin
from .models import Coin, Purchase
# Register your models here.

admin.site.register(Coin)
admin.site.register(Purchase)
