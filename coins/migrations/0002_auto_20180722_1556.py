# Generated by Django 2.0.7 on 2018-07-22 15:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('coins', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Purchase',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bought_at', models.DecimalField(decimal_places=4, max_digits=30)),
            ],
        ),
        migrations.AlterField(
            model_name='coin',
            name='current_price',
            field=models.DecimalField(decimal_places=4, max_digits=30),
        ),
        migrations.AddField(
            model_name='purchase',
            name='coin',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='coins.Coin'),
        ),
        migrations.AddField(
            model_name='purchase',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
