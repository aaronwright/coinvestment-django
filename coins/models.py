from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Coin(models.Model):
    name = models.CharField(max_length=50)
    ticker = models.CharField(max_length=5)
    current_price = models.DecimalField(max_digits=30, decimal_places=2)
    percent_change_24hr = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return self.name



class Purchase(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    bought_at = models.DecimalField(max_digits=30, decimal_places=2)
    amount = models.DecimalField(max_digits=30, decimal_places=8, default=.01)
    coin = models.ForeignKey(Coin, on_delete=models.CASCADE) 

    def __str__(self):
        return self.user.username