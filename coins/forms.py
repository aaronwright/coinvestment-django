from django.forms import ModelForm
from .models import Purchase
from django import forms


class PurchaseForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(PurchaseForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
    
    class Meta:
        model = Purchase
        fields = ['bought_at', 'amount']




class DeleteForm(forms.Form):
    id = forms.IntegerField(widget=forms.HiddenInput())